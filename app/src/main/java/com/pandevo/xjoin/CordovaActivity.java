package com.pandevo.xjoin;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.cordova.Config;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class CordovaActivity extends Activity implements CordovaInterface {
    private final ExecutorService threadPool = Executors.newCachedThreadPool();
    
    public static String TAG = "CordovaActivity";
    
    // Plugin to call when activity result is received
    protected CordovaPlugin activityResultCallback = null;
    protected boolean activityResultKeepRunning;    
    // Keep app running when pause is received. (default = true)
    // If true, then the JavaScript and native code continue to run in the background
    // when another application (activity) is started.
    protected boolean keepRunning = true;
    	
	CordovaWebView cwv;
	
	private String[] mMenuTitles;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    public static String mPageName = null;
    private LinearLayout mDrawer;
	
    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cordova);
        
        // Setting up the Cordova Web View and JavaScript Interface
        cwv = (CordovaWebView) findViewById(R.id.CordovaView);
        Config.init(this);
        //cwv.loadUrl(Config.getStartUrl());
        cwv.loadUrl("file:///android_asset/www/index.html#home");
        cwv.getSettings().setJavaScriptEnabled(true);
        cwv.addJavascriptInterface(new WebAppInterface(this), "Android");
                     
        // Initialize the navigation drawer's list of items 
        mMenuTitles = getResources().getStringArray(R.array.menu_array);
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawer = (LinearLayout) findViewById(R.id.drawer);
        
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mMenuTitles));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        /************** Open and Close with the App Icon **************************/
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
                
    }
    
    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawer);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...
        
        return super.onOptionsItemSelected(item);
    }
    
    
    
    
    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
         
    }
    
    /** Changes cordova (jQuery) pages in the main content view */
    private void selectItem(int position) {
        // Determine the page id according to selected item
    	//mPageName = "page" + position;
    	
    	switch (position) {
    		case 0:	mPageName = "favs";
    				break;
    		case 1:	mPageName = "jobsearch";
					break;
    		case 2:	mPageName = "wagecheck";
					break;
    		case 3:	mPageName = "guestwagecheck";
					break;
    		case 4: mPageName = "wageresult";
    				break;
    		case 5: mPageName = "jobresults";
    				break;
    		case 6: mPageName = "jobdetails";
    				break;
    	}
       
        // Change the page by calling a jQuery JavaScript function 
        cwv.sendJavascript("selectPage(\""+mPageName+"\");");
       
        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawer);
    }
    
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    
    
  
/************************************ Apache Cordova Settins ************************************/
    
    
	@Override
	 public Activity getActivity() {
        return this;
    }
	
	
	/**
     * Called when a message is sent to plugin.
     *
     * @param message            The message id
     * @param obj          The message data
     * @return              Object or null
     */
	@Override
	public Object onMessage(String message, Object obj) {
		Log.d(TAG, message);
		if (message.equalsIgnoreCase("exit")) {
			super.finish();
		}
		return null;
	}
	
	@Override
    public void setActivityResultCallback(CordovaPlugin plugin) {
        this.activityResultCallback = plugin;
    }
	/**
	 * Launch an activity for which you would like a result when it finished. When this activity exits,
	 * your onActivityResult() method is called.
	 *
	 * @param command           The command object
	 * @param intent            The intent to start
	 * @param requestCode       The request code that is passed to callback to identify the activity
	 */
	
	@Override
	public void startActivityForResult(CordovaPlugin command, Intent intent, int requestCode) {
        this.activityResultCallback = command;
        this.activityResultKeepRunning = this.keepRunning;

        // If multitasking turned on, then disable it for activities that return results
        if (command != null) {
            this.keepRunning = false;
        }

        // Start activity
        super.startActivityForResult(intent, requestCode);
    }
	
	 /**
     * Cancel loadUrl before it has been loaded.
     */
    @Deprecated
    public void cancelLoadUrl() {
    	// This is a no-op.
    }
	
	@Override
	 public ExecutorService getThreadPool() {
       return threadPool;
   }
    
    
	@Override
	/**
	 * Called when an activity you launched exits, giving you the requestCode you started it with,
	 * the resultCode it returned, and any additional data from it.
	 *
	 * @param requestCode       The request code originally supplied to startActivityForResult(),
	 *                          allowing you to identify who this result came from.
	 * @param resultCode        The integer result code returned by the child activity through its setResult().
	 * @param data              An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
	    super.onActivityResult(requestCode, resultCode, intent);
	    CordovaPlugin callback = this.activityResultCallback;
	    if (callback != null) {
	        callback.onActivityResult(requestCode, resultCode, intent);
	    }	
	}
	
	  @Override
	    /**
	     * Called when the system is about to start resuming a previous activity.
	     */
	    protected void onPause() {
	        super.onPause();
	 
	         // Send pause event to JavaScript
	        this.cwv.loadUrl("javascript:try{cordova.fireDocumentEvent('pause');}catch(e){console.log('exception firing pause event from native');};");
	 
	        // Forward to plugins
	        if (this.cwv.pluginManager != null) {
	            this.cwv.pluginManager.onPause(true);
	        }
	    }
	 
	    @Override
	    /**
	     * Called when the activity will start interacting with the user.
	     */
	    protected void onResume() {
	        super.onResume();
	 
	        if (this.cwv == null) {
	            return;
	        }
	 
	        // Send resume event to JavaScript
	        this.cwv.loadUrl("javascript:try{cordova.fireDocumentEvent('resume');}catch(e){console.log('exception firing resume event from native');};");
	 
	        // Forward to plugins
	        if (this.cwv.pluginManager != null) {
	            this.cwv.pluginManager.onResume(true);
	        }
	 
	    }
	 
	    @Override
	    /**
	     * The final call you receive before your activity is destroyed.
	     */
	    public void onDestroy() {
	        LOG.d(TAG, "onDestroy()");
	        super.onDestroy();
	 
	        if (this.cwv != null) {
	 
	            // Send destroy event to JavaScript
	            this.cwv.loadUrl("javascript:try{cordova.require('cordova/channel').onDestroy.fire();}catch(e){console.log('exception firing destroy event from native');};");
	 
	            // Load blank page so that JavaScript onunload is called
	            this.cwv.loadUrl("about:blank");
	            cwv.handleDestroy();
	        }
	    }
	    
	    @Override
	    /**
	     * Called when the activity receives a new intent
	     **/
	    protected void onNewIntent(Intent intent) {
	        super.onNewIntent(intent);
	 
	        //Forward to plugins
	        if ((this.cwv != null) && (this.cwv.pluginManager != null)) {
	            this.cwv.pluginManager.onNewIntent(intent);
	        }
	    }
}
