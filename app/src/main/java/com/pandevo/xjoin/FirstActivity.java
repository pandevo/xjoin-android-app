package com.pandevo.xjoin;

import android.R.drawable;
import android.app.Activity;
import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class FirstActivity extends Activity {

	private Button mLoginButton;
	private Button mRegisterButton;
	private Button mGuestButton;
	PaintDrawable paint = new PaintDrawable();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		mLoginButton = (Button) findViewById(R.id.login_btn);
		mLoginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if (view.getId() == R.id.login_btn){
					startLoginActivity();
			}
			}
		});
		
		mRegisterButton = (Button)findViewById(R.id.register_btn);
		mRegisterButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if (view.getId() == R.id.register_btn){
					startRegisterActivity();
			}
			}	
		});
		mRegisterButton.setBackgroundDrawable(paint);
		
		mGuestButton = (Button)findViewById(R.id.guest_btn);
		mGuestButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if (view.getId() == R.id.guest_btn){
					startCordovaActivity();
			}
			}	
		});
		
		/** Golden gradient for register button **/
		ShapeDrawable.ShaderFactory shaderFactory = new ShapeDrawable.ShaderFactory() {
		    @Override
		    public Shader resize(int width, int height) {		    			    	
		        LinearGradient linearGradient = new LinearGradient(0, 0, width, width,
		            new int[] { 
		                0xFFd4b685,
		                0xFFd4b685,
		                0xFFb89d73, 
		                0xFFe6cda8, 
		                0xFFe8c792,
		                0xFFe8c792}, 
		             /**   0xFF000000,
		                0xFFffffff,
		                0xFF000000, 
		                0xFFffffff, 
		                0xFF000000,
		                0xFFffffff}, **/
		            new float[] {
		                0, 0.07f, 0.20f, 0.33f, 0.47f, 1 }, //positions
		            Shader.TileMode.REPEAT);
		         return linearGradient;
		    }
		};
		
		paint.setShape(new RectShape());
		paint.setShaderFactory(shaderFactory);
		
	}

	private void startLoginActivity() {
		Intent intent = new Intent(getApplicationContext(), Login.class);
		startActivity(intent);		
	}
	
	private void startRegisterActivity() {
		Intent intent = new Intent(getApplicationContext(), Register.class);
		startActivity(intent);		
	}
	
	private void startCordovaActivity() {
		Intent intent = new Intent(getApplicationContext(), CordovaActivity.class);
		startActivity(intent);
	}

}