package com.pandevo.xjoin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NativeActivity extends Activity {

	private Button start_cordova_activity_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_native);
		start_cordova_activity_btn = (Button) findViewById(R.id.invoke_cordova_activity_btn);
		start_cordova_activity_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if (view.getId() == R.id.invoke_cordova_activity_btn){
					startCordovaActivity();
			}
			}
		});
	}

	private void startCordovaActivity() {
		Intent intent = new Intent(getApplicationContext(), CordovaActivity.class);
		startActivity(intent);		
	}

}
