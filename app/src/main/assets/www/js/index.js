/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
initialize: function() {
this.bind();
},
bind: function() {
document.addEventListener('deviceready',
this.deviceready, false);
},
deviceready: function() {
//Note that this is an event handler so the scope is
//that of the event so we need to call app.report(),
//and not this.report()
app.report('deviceready');
},
report: function(id) {
console.log("report:" + id);
//Hide the .pending <p> and show the .complete <p>
document.querySelector('#' + id + ' .pending').className
+= ' hide';
var completeElem =
document.querySelector('#' + id + ' .complete');
completeElem.className =
completeElem.className.split('hide').join('');

}
};